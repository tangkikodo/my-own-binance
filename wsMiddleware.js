// reference: http://blog.finjs.io/2017/02/16/handle-realtime-streaming-data-with-redux/
let websocket;

export const middleware = ({ dispatch }) => {
  return next => action => {
    switch (action.type) {
      case "WEBSOCKET:CONNECT":
        if (websocket) {
            break
        }
        websocket = new WebSocket(action.payload.url);

        websocket.onopen = () => dispatch({ type: "WEBSOCKET:OPEN" });
        // websocket.onclose = event =>
        //   dispatch({ type: "WEBSOCKET:CLOSE", payload: event });
        websocket.onmessage = event =>
          dispatch({ type: "WEBSOCKET:MESSAGE", payload: event });

        break;

      case "WEBSOCKET:DISCONNECT":
        if (websocket) {
            websocket.close();
            websocket = null;
        }
        break;

      default:
        break;
    }
    return next(action);
  };
};
