docker
```shell
docker run -v ${pwd}:/app -i -t node:8 /bin/bash

cd /app
node server.js
```

or 

```shell
npm i
node server.js
```

[x] redux introduced

[x] marketing pairs

[x] enable/disable ws

[x] select pair

[x] display selected pair in trade page

[ ] trade, buy/sell   (failed to figure out the rest-api of order ... always get 40x)