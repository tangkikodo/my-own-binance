import React from "react";
import { store, setMarketFilter } from "../store";
import { Provider } from "react-redux";
import TradeBoard from "../components/tradeBoard";
import { withRouter } from "next/router";

import axios from "axios";

function generateSignature(trade) {
  let timestamp = +new Date();
  let requestBody = `${trade}&timestamp=${timestamp}`;

  return axios
    .post("/api/v1/signature", { s: requestBody })
    .then(res => res.data);
}

class Trade extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    store.dispatch(setMarketFilter(this.props.router.query.symbol));

    let s =
      "symbol=LTCBTC&side=BUY&type=LIMIT&timeInForce=GTC&quantity=1&price=0.2&recvWindow=5000";
    axios.post(`/api/v1/buy?${s}`).then(res => console.log(res));
  }

  render() {
    return (
      <Provider store={store}>
        <TradeBoard />
      </Provider>
    );
  }
}

export default withRouter(Trade);
