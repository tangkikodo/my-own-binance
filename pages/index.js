import React from "react";
import { store } from "../store";
import { Provider } from "react-redux";
import Board from "../components/board";


class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Provider store={store}>
        <Board />
      </Provider>
    );
  }
}

export default Home;
