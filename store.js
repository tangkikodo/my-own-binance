import { createStore, applyMiddleware } from "redux";
import { combineReducers } from "redux";
import thunkMiddleware from "redux-thunk";
import { middleware } from "./wsMiddleware";

// helper variables
let uniqCache = {};

//initial store
const initialState = {};

// actions
export const connectWebsocket = (
  url = "wss://stream.binance.com:9443/stream?streams=!miniTicker@arr@3000ms"
) => ({
  type: "WEBSOCKET:CONNECT",
  payload: { url }
});

export const stopWebsocket = () => ({
  type: 'WEBSOCKET:DISCONNECT'
})

export const MarketFilter = {
  BNB: "BNB",
  BTC: "BTC",
  ETH: "ETH",
  USDT: "USDT"
};

export const setMarketFilter = filter => ({
  type: "SET_MARKET_FILTER",
  filter
});

export const updatePairs = pairs => ({
  type: "UPDATE_PAIRS",
  pairs
});

export const toggleWebsocket = () => ({
  type: "TOGGLE_WEBSOCKET"
});

// reducers

const pairs = (state = [], action) => {
  switch (action.type) {
    case "UPDATE_PAIRS":
      return action.pairs;

    case "WEBSOCKET:MESSAGE":
      let data = JSON.parse(action.payload.data);
      let pairs = data.data;
      pairs.reduce((prev, item) => {
        let symbol = item.s;

        if (prev[symbol]) {
          if (prev[symbol].E < item.E) {
            prev[symbol] = item;
          }
        } else {
          prev[symbol] = item;
        }
        return prev;
      }, uniqCache);

      let uniq = Object.keys(uniqCache).map(k => uniqCache[k]);
      return uniq;

    default:
      return state;
  }
};

const marketFilter = (state = MarketFilter.BTC, action) => {
  switch (action.type) {
    case "SET_MARKET_FILTER":
      return action.filter;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  pairs,
  marketFilter
});

export const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(thunkMiddleware, middleware)
);
