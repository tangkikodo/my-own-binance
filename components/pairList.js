import PairItem from "./pairItem";
import { connect } from "react-redux";

const getPairs = (pairs, filter) => {
  let symbolChecker = new RegExp(`${filter}$`);
  return pairs.filter(p => symbolChecker.test(p.s));
};

const mapStateToProps = state => ({
    pairs: getPairs(state.pairs, state.marketFilter),
})

const PairList = ({pairs}) => {
  const Items = pairs.map(p => <PairItem key={p.s} pair={p} />);

  return (
    <table className="table">
      <thead>
        <tr>
          <th>mkt</th>
          <th>latest price</th>
          <th>24h high</th>
          <th>24h low</th>
          <th>24h volumn</th>
          <th>Operation</th>
        </tr>
      </thead>
      <tbody>{Items}</tbody>
    </table>
  );
};

export default connect(mapStateToProps)(PairList);
