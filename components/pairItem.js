import Link from "next/link";
import { connect } from "react-redux";

const mapStateToProps = state => ({
  filter: state.marketFilter
});

const PairItem = ({ pair, filter }) => (
  <tr>
    <td> {pair.s} </td>
    <td> {pair.c} </td>
    <td> {pair.h} </td>
    <td> {pair.l} </td>
    <td> {pair.v} </td>
    <td>
      <Link href={`/trade?symbol=${filter}`}>
        <a>trade</a>
      </Link>
    </td>
  </tr>
);

export default connect(mapStateToProps)(PairItem);
