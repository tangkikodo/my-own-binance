import { stopWebsocket, connectWebsocket } from "../store";
import { connect } from "react-redux";

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  stop: () => dispatch(stopWebsocket()),
  connect: () => dispatch(connectWebsocket())
});

const ToggleWs = ({ stop, connect }) => {
  return (
    <div>
      <button onClick={stop}>close ws</button>
      <button onClick={connect}>reconnect ws</button>
    </div>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ToggleWs);
