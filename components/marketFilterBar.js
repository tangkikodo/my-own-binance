import { setMarketFilter } from "../store";
import { connect } from "react-redux";

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  changeFilter: filter => dispatch(setMarketFilter(filter))
});

const MarketFilterBar = ({changeFilter}) => (
  <div>
    <button onClick={() => changeFilter("BTC")}>BTC</button>
    <button onClick={() => changeFilter("BNB")}>BNB</button>
    <button onClick={() => changeFilter("ETH")}>ETH</button>
    <button onClick={() => changeFilter("USDT")}>USDT</button>
  </div>
);

export default connect(mapStateToProps, mapDispatchToProps)(MarketFilterBar);
