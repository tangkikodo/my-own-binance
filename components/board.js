import React from "react";
import { connect } from "react-redux";
import PairList from "./pairList";
import MarketFilterBar from "./marketFilterBar";
import ToggleWs from "./toggleWs";
import { connectWebsocket, stopWebsocket } from "../store";

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  connectWebsocket: () => dispatch(connectWebsocket()),
  disconnectWebsocket: () => dispatch(stopWebsocket())
});

class Board extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.connectWebsocket();
  }

  componentWillUnmount() {
    this.props.disconnectWebsocket();
  }
  render() {
    return (
      <div>
        <ToggleWs />
        <br />
        <MarketFilterBar />
        <PairList />
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Board);
