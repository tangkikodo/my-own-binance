import React from "react";
import { connect } from "react-redux";
import PairList from "./pairList";
import {connectWebsocket, stopWebsocket, setMarketFilter} from '../store'

const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({
  connectWebsocket: () => dispatch(connectWebsocket()),
  disconnectWebsocket: () => dispatch(stopWebsocket()),
  // setMarketFilter: filter => dispatch(setMarketFilter(filter)),
})

class TradeBoard extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.connectWebsocket();
  }
  componentWillUnmount() {
    this.props.disconnectWebsocket();
  }

  render() {
    return (
      <div >
        <PairList />
      </div>
    );
  }
}

// TradeBoard.getInitialProps = ({req}) => {
//   console.log(req.query)
//   this.props.setMarketFilter(req.query.symbol)
// }

export default connect(mapStateToProps, mapDispatchToProps)(TradeBoard);
