const exec = require("child_process").exec;
const bodyParser = require("body-parser");
const express = require("express");
const next = require("next");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const axios = require("axios");

let axiosConfig = {
  baseURL: "https://api.binance.com",
  headers: {
    "X-MBX-APIKEY":
      "vmPUZE6mv9SD5VNHk4HlWFsOr6aKE2zvsw0MuIgwCIPy6utIco14y7Ju91duEh8A"
  }
};

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(bodyParser.json());

    // server.post("/api/v1/signature", (req, res) => {
    //   let s = req.body.s;
    //   const script = exec(
    //     `echo -n "${s}" | openssl dgst -sha256 -hmac "NhqPtmdSJYdKjVHjA7PZj4Mge3R5YNiP1e3UZjInClVN65XAbvqqM6A7H5fATj0j"`
    //   );
    //   script.stdout.on("data", data => {
    //     let ret = data.split(" ")[1];
    //     ret = ret.replace(/[\n\r]+/g, "");
    //     res.send({
    //       signature: ret,
    //       fullQuery: `${s}&signature=${ret}`
    //     });
    //   });
    // });

    server.post("/api/v1/buy", (req, res) => {
      let s = req.body.s;
      const script = exec(
        `echo -n "${s}" | openssl dgst -sha256 -hmac "NhqPtmdSJYdKjVHjA7PZj4Mge3R5YNiP1e3UZjInClVN65XAbvqqM6A7H5fATj0j"`
      );
      script.stdout.on("data", data => {
        let ret;
        if (data.split(' ').length > 1) {

          ret = data.split(" ")[1];
        }
        else {
          ret = data;

        }
        ret = ret.replace(/[\n\r]+/g, "");
        axios
          .post(
            `https://api.binance.com/api/v3/order?${s}&signature=${ret}`,
            {},
            axiosConfig
          )
          .then(
            data => res.send(data),
            err => {
              console.log(err);
              res.status(400).end();
            }
          );
      });
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(3000, err => {
      if (err) throw err;
      console.log("> Ready on http://localhost:3000");
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
